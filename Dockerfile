ARG PIPELINE_ID=0

FROM nginx
COPY index.html /usr/share/nginx/html/index.html

# we need a random layer so images sha are different on each gitlab build
RUN echo "build by pipeline: $PIPELINE_ID" > /.build_with
